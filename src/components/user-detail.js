import React, { Component } from 'react';
import { View, FlatList, StyleSheet , SafeAreaView, Image} from 'react-native';
import axios from 'axios';

export default class UserDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const photo = navigation.getParam('photo', {});

    axios.get(`http://localhost:3000/users/${photo.id}`)
      .then(response => {
        console.log(JSON.stringify(response));
        this.setState({ data: response.data });
      });
  }

  render() {
    const { navigation } = this.props;
    const photo = navigation.getParam('photo', {});
    const columns = 3;

    console.log(`${JSON.stringify(photo)}`);
    
    return (
      <SafeAreaView>

         <FlatList
          data={this.state.data.images}
          numColumns={2}
          keyExtractor={(item, index) => index}
          renderItem={({ item }) => {
            return (
              <Image source={{uri: item}} style={styles.image} />
            );
          }}
        />

      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    alignItems: "center",
    flexGrow: 1,
    margin: 4,
    padding: 20,
    flexBasis: 0,
    width: 160, 
    height: 180
  },
  text: {
    color: "#333333"
  }, 
});
