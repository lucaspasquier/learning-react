import { createStackNavigator } from 'react-navigation';
import  PhotoFeed  from './components/photo-feed';
import  UserDetail from './components/user-detail';

export default RootStack = createStackNavigator({
    Home: PhotoFeed,
    Detail: UserDetail
}, {initialRouteName: 'Home'});